# Distributed under the The 3-clause BSD (aka BSD License 2.0)
# See LICENSE.md in repository root for more information.

import os
import string

from flask import Blueprint, jsonify, request, g, url_for
from jsonschema import validate
import requests
from uuid import uuid4

from erudite.db import get_db
from erudite.error import NotFoundException
from erudite.auth import auth
from erudite.author import add_author_to_db, search_authors_by_name
from erudite.publisher import add_publisher_to_db, search_publisher_by_name
from erudite.upload import save_file, generate_filename, allowed_file
import erudite.googlebooks as gb
import random

bp = Blueprint('book', __name__, url_prefix='/books')

schema = {
    "type": "object",
    "properties": {
        "title": {
            "type": "string",
        },
        "authors": {
            "type": "array",
            "items": {
                "type": "integer",
            }
        },
        "isbn": {
            "type": "string",
        },
        "pages": {
            "type": "integer",
        },
        "year": {
            "type": "integer",
        },
        "id_publisher": {
            "type": "integer",
        },
        "description": {
            "type": "string",
        },
        "tags": {
            "type": "array",
            "items": {
                "type": "integer",
            }
        },
    },
    "required": [
        "title", "authors", "isbn"
        ]
}

schema_review = {
    "type": "object",
    "properties": {
        "content": {
            "type": "string",
        },
    },
    "required": [
        "content"
    ]
}


class BookNotFoundException(NotFoundException):
    def __init__(self, idx):
        super().__init__("book", idx)


def fetch_book(book_id: int):
    """
    Fetch a single book from database

    Parameters
    ----------
    book_id : int
        ID of book

    Returns
    -------
    dict
        dictionary representing fetched book
    """

    db = get_db()
    sql = """
    SELECT id_book, title, isbn, pages, year, id_publisher, description, cover
    FROM book WHERE id_book = %s
    """
    db.execute(sql, (book_id,))
    book = db.fetchone()
    if book is None:
        raise BookNotFoundException(book_id)

    if "id_publisher" in book:
        sql = "SELECT id_publisher, name FROM publisher WHERE id_publisher = %s"
        db.execute(sql, (book["id_publisher"],))
        book["publisher"] = db.fetchone()
        del book["id_publisher"]

    sql = "SELECT id_author, name FROM book_author JOIN author USING (id_author) WHERE id_book = %s"
    db.execute(sql, (book_id,))
    book['authors'] = db.fetchall()

    sql = """
    SELECT t.id_tag, t.color, t.name FROM tag t
    JOIN book_tag bt USING (id_tag)
    WHERE id_book = %s
    """
    db.execute(sql, (book["id_book"],))
    book['tags'] = db.fetchall()
    return book


def fetch_random_books():
    """
    Fetch all books from database
    Returns
    -------
    list
        list of dictionaries representing all books
    """
    db = get_db()
    sql = """
    SELECT id_book, title, isbn, pages, year, id_publisher, description, cover
    FROM book
    ORDER BY RANDOM()
    """
    db.execute(sql)
    books = db.fetchall()
    for book in books:
        if "id_publisher" in book:
            sql = "SELECT id_publisher, name FROM publisher WHERE id_publisher = %s"
            db.execute(sql, (book["id_publisher"],))
            book["publisher"] = db.fetchone()
            del book["id_publisher"]
        sql = "SELECT id_author, name FROM book_author JOIN author USING (id_author) WHERE id_book = %s"
        db.execute(sql, (book["id_book"],))
        book['authors'] = db.fetchall()
        sql = """
        SELECT t.id_tag, t.color, t.name FROM tag t
        JOIN book_tag bt USING (id_tag)
        WHERE id_book = %s
        """
        db.execute(sql, (book["id_book"],))
        book['tags'] = db.fetchall()
    return books

def search_books(title: str = "", author: str = "", isbn: str = ""):
    """
    Search books by title, author and/or ISBN
    
    Parameters
    ----------
    title : str
        Title of book
    author : str
        Author of book
    isbn : str
        ISBN of book
    
    Returns
    -------
    list
        list of books matching search criteria
    """
    db = get_db()
    sql = """
    SELECT DISTINCT b.id_book, b.title, b.cover FROM book b
    JOIN book_author ba ON b.id_book = ba.id_book
    JOIN author a ON ba.id_author = a.id_author
    WHERE b.title ilike '%%' || %s || '%%' AND
          a.name ilike '%%' || %s || '%%' AND
          %s in (b.isbn, '')
    ORDER BY b.id_book
    """
    db.execute(sql, (title, author, isbn))
    books = db.fetchall()
    for book in books:
        sql = """
        SELECT a.id_author, a.name FROM author a
        JOIN book_author ba USING (id_author)
        WHERE ba.id_book = %s
        """
        db.execute(sql, (book["id_book"],))
        book["authors"] = db.fetchall()
    return books

def search_books_by_string(s: str):
    """
    Search books by title, author and/or ISBN

    Parameters
    ----------
    s : str
        Search string may represent (title, author or ISBN)

    Returns
    -------
    list
        list of books matching search criteria
    """
    db = get_db()
    sql = """
    SELECT DISTINCT b.id_book, b.title, b.cover FROM book b
    JOIN book_author ba ON b.id_book = ba.id_book
    JOIN author a ON ba.id_author = a.id_author
    WHERE b.title ilike '%%' || %(ss)s || '%%' OR
          a.name ilike '%%' || %(ss)s || '%%' OR
          %(ss)s in (b.isbn, '')
    ORDER BY b.id_book
    """
    db.execute(sql, {"ss": s})
    books = db.fetchall()
    for book in books:
        sql = """
        SELECT a.id_author, a.name FROM author a
        JOIN book_author ba USING (id_author)
        WHERE ba.id_book = %s
        """
        db.execute(sql, (book["id_book"],))
        book["authors"] = db.fetchall()
    return books

def add_book(book: dict):
    """
    Add a new book to database
    
    Parameters
    ----------
    book : dict
        dictionary representing book
    """
    db = get_db()
    sql = """
    INSERT INTO book (title, isbn, pages, year, id_publisher, description)
    VALUES (%(title)s, %(isbn)s, %(pages)s, %(year)s, %(id_publisher)s, %(description)s)
    RETURNING id_book
    """
    db.execute(sql, book)
    id_book = db.fetchone()["id_book"]
    sql = """
    INSERT INTO book_author (id_book, id_author)
    VALUES (%s, %s)
    """
    for author in book["authors"]:
        db.execute(sql, (id_book, author))
    sql = """
    INSERT INTO book_tag (id_book, id_tag)
    VALUES (%s, %s)
    """
    for tag in book["tags"]:
        db.execute(sql, (id_book, tag))
    shelf_mark = ''.join(random.choice(string.ascii_uppercase))
    shelf_mark = shelf_mark.join(random.choice(string.digits))
    shelf_mark = shelf_mark.join(random.choice(string.digits))
    sql = """
        INSERT INTO catalog (id_book, shelf_mark)
        VALUES (%s, %s)
        """
    db.execute(sql, (id_book, shelf_mark,))

def update_book(book):
    db = get_db()
    sql = """
    UPDATE book
    SET title = %(title)s, isbn = %(isbn)s, pages = %(pages)s, year = %(year)s, id_publisher = %(id_publisher)s, description = %(description)s
    WHERE id_book = %(id_book)s
    """
    db.execute(sql, book)
    sql = """
    DELETE FROM book_author
    WHERE id_book = %s
    """
    db.execute(sql, (book["id_book"],))
    sql = """
    INSERT INTO book_author (id_book, id_author)
    VALUES (%s, %s)
    """
    for author in book["authors"]:
        db.execute(sql, (book["id_book"], author))
    sql = """
    DELETE FROM book_tag
    WHERE id_book = %s
    """
    db.execute(sql, (book["id_book"],))
    sql = """
    INSERT INTO book_tag (id_book, id_tag)
    VALUES (%s, %s)
    """
    for tag in book["tags"]:
        db.execute(sql, (book["id_book"], tag))

def add_review_to_a_book(book_id, id_user, review: dict):
    """
    Add a new review to a book
    
    Parameters
    ----------
    book_id : int
        ID of book    
    id_user : int
        ID of user
    review : dict
        dictionary representing review

    Raises
    ------
    BookNotFoundException
        If book with given ID does not exist
    """
    if fetch_book(book_id) is None:
        raise BookNotFoundException(book_id)
    #baza danych się wywalała ze wzglegu na primary key, więc musiałem to zrobić
    db = get_db()
    sql = """
    INSERT INTO review (content, id_user, id_book)
    VALUES (%s, %s, %s)
    """
    db.execute(sql, (review["content"], id_user, book_id,))

def add_cover_to_book(book_id, cover: str):
    """
    Add cover to book
    
    Parameters
    ----------
    book_id : int
        ID of book
    cover : str
        cover of book in string format(link to image)
    Raises
    ------
    BookNotFoundException
        If book with given ID does not exist
    """
    fetch_book(book_id) 
    cover = "/static/uploads/" + cover
    db = get_db()
    sql = """
    UPDATE book SET cover = %s WHERE id_book = %s
    """
    db.execute(sql, (cover, book_id))


@bp.get("/<int:book_id>")
def get_one_book_endpoint(book_id):
    book = fetch_book(book_id)
    return jsonify(book)

@bp.get("")
def get_random_books_endpoint():
    books = fetch_random_books()
    return jsonify(books)

@bp.get("/search")
def search_books_endpoint():
    args = request.args
    books = dict()
    s = args.get("s")
    if s is None:
        title = args.get("title", "")
        author = args.get("author", "")
        isbn = args.get("isbn", "")
        books = search_books(title, author, isbn)
    else:
        books = search_books_by_string(s)
    return jsonify(books)

@bp.post("/<int:book_id>/review")
@auth.login_required(role="client")
def post_review_endpoint(book_id):
    data = request.get_json()
    validate(data, schema_review)
    id_user = auth.current_user()["id_user"]
    add_review_to_a_book(book_id, id_user, data)
    return '', 204
    

@bp.post("")
@auth.login_required(role="librarian")
def post_book_endpoint():
    data = request.get_json()
    validate(data, schema)
    add_book(data)
    return '', 204

@bp.put("/<int:book_id>")
@auth.login_required(role="librarian")
def put_book_endpoint(book_id):
    data = request.get_json()
    validate(data, schema)
    data["id_book"] = book_id
    update_book(data)
    return '', 204

@bp.post("/<int:book_id>/upload_cover")
@auth.login_required(role="librarian")
def post_cover_endpoint(book_id):
    if 'file' not in request.files:
        return '', 400
    file = request.files['file']
    if (allowed_file(file.filename) == False):
        return '', 418
    file_name = generate_filename(file.filename)
    save_file(file, file_name)
    add_cover_to_book(book_id, file_name)
    return '', 204

@bp.post("/add_with_isbn")
@auth.login_required(role="librarian")
def post_book_with_isbn_endpoint():
    args = request.args
    if "isbn" not in args:
        return 'isbn query parameter is required', 400
    book = gb.fetch_book_by_isbn(args["isbn"])
    if book is None:
        return 'Book not found in Google Books database', 404
    _add_authors(book)
    _add_publisher(book)
    _download_cover(book)
    book['tags'] = list()
    add_book(book)
    return jsonify(book)

def _add_authors(book):
    authors = list()
    for author in book['authors']:
        db_authors = search_authors_by_name(author)
        if len(db_authors) == 0:
            add_author_to_db({"name": author})
            added_author = search_authors_by_name(author)[0]
            authors.append(added_author["id_author"])
        else:
            authors.append(db_authors[0]["id_author"])
    book['authors'] = authors

def _add_publisher(book):
    publisher = book["publisher"]
    del book["publisher"]
    if publisher is None:
        book["id_publisher"] = None
        return
    db_publisher = search_publisher_by_name(publisher)
    if db_publisher is None:
        add_publisher_to_db({"name": publisher})
        added_publisher = search_publisher_by_name(publisher)
        book["id_publisher"] = added_publisher["id_publisher"]
    else:
        book["id_publisher"] = db_publisher["id_publisher"]




def _download_cover(book):
    if 'cover' not in book:
        return
    r = requests.get(book['cover'])
    filename = str(uuid4()) + ".jpg"
    book['cover'] = url_for('static', filename=f'uploads/{filename}')
    filepath = os.path.join(os.path.dirname(__file__), 'static', 'uploads', filename)
    with open(filepath, 'wb') as fp:
        fp.write(r.content)
