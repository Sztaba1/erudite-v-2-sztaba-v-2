# Distributed under the The 3-clause BSD (aka BSD License 2.0)
# See LICENSE.md in repository root for more information.

from flask import Blueprint, Response, jsonify, request
from jsonschema import validate

from erudite.db import get_db
from erudite.error import NotFoundException
from erudite.auth import auth
from erudite.parse import parse_id_from_query

bp = Blueprint('catalog', __name__, url_prefix='/catalog')
schema_reservation = {
    "type": "object",
    "properties": {
        "receipt_term": {
            "type": "string",
            "format": "date"
        },
        "submitted": {
            "type": "string",
            "format": "date"
        },
        "id_catalog": {
            "type": "int"
        },
        "id_user": {
            "type": "int"
        }
    },
    "required": [
        "id_catalog", "id_user"
    ]
}
def fetch_catalog_for_book(id_book):
    db = get_db()
    sql = """
    SELECT id_catalog, id_book, shelf_mark
    FROM catalog
    WHERE id_book = %s
    ORDER BY id_catalog
    """
    db.execute(sql, (id_book,))
    return db.fetchall()

def post_new_reservation(id_catalog: int, id_user: int):
    db = get_db()
    sql = """
        INSERT INTO reservation(
        id_user, id_catalog)
        VALUES (%s, %s)
        """
    db.execute(sql, (id_user, id_catalog, ))

@bp.get("/get_for_book")
def get_catalog_for_book_endpoint():
    id_book = parse_id_from_query("book", request.args)
    catalog = fetch_catalog_for_book(id_book)
    return jsonify(catalog)

@bp.post("/<int:catalog_id>/reserve")
@auth.login_required(role = 'client')
def reserve_book_from_catalog_endpoint(catalog_id: int):
    user_id = auth.current_user()["id_user"]
    post_new_reservation(catalog_id, user_id)
    return '', 204
