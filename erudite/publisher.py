# Distributed under the The 3-clause BSD (aka BSD License 2.0)
# See LICENSE.md in repository root for more information.

from flask import Blueprint, Response, jsonify, request
from jsonschema import validate

from erudite.db import get_db
from erudite.error import NotFoundException
from erudite.auth import auth

bp = Blueprint('publisher', __name__, url_prefix='/publishers')
schema = {
    "type": "object",
    "properties": {
        "name": {
            "type": "string",
        }
    },
    "required": [
        "name"
    ]
}

class PublisherNotFoundException(NotFoundException):
    def __init__(self, idx):
        super().__init__("publisher", idx)

def fetch_publisher(publisher_id: int) -> dict:
    """
    Fetch a single publisher from database

    Parameters
    ----------
    publisher_id : int
        ID of publisher

    Returns
    -------
    dict
        dictionary representing fetched publisher
    """

    db = get_db()
    sql = """SELECT 
    id_publisher, name
    FROM publisher WHERE id_publisher = %s"""
    db.execute(sql, (publisher_id,))

    publisher = db.fetchone()

    if publisher is None:
        raise PublisherNotFoundException(publisher_id)
    return publisher


def fetch_all_publishers() -> list:
    """
    Fetch all publishers from database

    Returns
    -------
    list
        list of dictionaries representing publishers
    """

    db = get_db()
    sql = """SELECT 
    id_publisher, name
    FROM publisher"""
    db.execute(sql)
    publishers = db.fetchall()
    return publishers

def search_publisher_by_name(name: str):
    db = get_db()
    sql = """
    SELECT id_publisher, name FROM publisher
    WHERE name ilike '%%' || %s || '%%'
    """
    db.execute(sql, (name,))
    return db.fetchone()

def fetch_books_for_publisher(publisher_id: int) -> list:
    """
    Fetch all books for a publisher from database

    Returns
    -------
    list
        list of dictionaries representing books
    """

    db = get_db()
    sql = """SELECT 
    id_book, title, isbn, pages, year, id_publisher, description, cover
    FROM book WHERE id_publisher = %s"""
    db.execute(sql, (publisher_id,))
    books = db.fetchall()

    for book in books:
        if "id_publisher" in book:
            sql = """SELECT
             id_publisher, name 
             FROM publisher WHERE id_publisher = %s"""
            db.execute(sql, (book["id_publisher"],))
            book["publisher"] = db.fetchone()
            del book["id_publisher"]
    
    for book in books:
        if "id_book" in book:
            sql = """SELECT
            id_author, name 
            FROM book_author JOIN author USING (id_author) WHERE id_book = %s"""
            db.execute(sql, (book["id_book"],))
            book["authors"] = db.fetchall()

    return books

def add_publisher_to_db(publisher: dict):
    validate(instance=publisher, schema=schema)
    db = get_db()
    sql = """
    INSERT INTO publisher(name)
    VALUES (%(name)s)
    """
    db.execute(sql, publisher)

def remove_publisher_from_db(publisher_id: int):
    db = get_db()
    sql = """
    UPDATE book SET id_publisher = NULL WHERE id_publisher = %s
    """
    db.execute(sql, (publisher_id,))
    sql = """
    DELETE FROM publisher WHERE id_publisher = %s
    """
    db.execute(sql, (publisher_id,))

def update_publisher(publisher_id: int, publisher: dict):
    db = get_db()
    fetch_publisher(publisher_id)
    sql = """
    UPDATE publisher SET name = %s WHERE id_publisher = %s
    """
    db.execute(sql, (publisher["name"], publisher_id,))

@bp.get("/<int:publisher_id>")
def get_publisher(publisher_id: int) -> Response:
        publisher = fetch_publisher(publisher_id)

        return jsonify(publisher)
@bp.get("/<int:publisher_id>/get_books")
def get_books_for_publisher(publisher_id: int) -> Response:
        books = fetch_books_for_publisher(publisher_id)
        return jsonify(books)

@bp.post("")
@auth.login_required(role = 'librarian')
def post_publisher_endpoint():
    publisher = request.get_json()
    add_publisher_to_db(publisher)
    return '', 204
@bp.delete("/<int:publisher_id>")
@auth.login_required(role = 'librarian')
def delete_publisher_endpoint(publisher_id: int):
    remove_publisher_from_db(publisher_id)
    return '', 204

@bp.put("/<int:publisher_id>")
@auth.login_required(role = 'librarian')
def put_publisher_endpoint(publisher_id: int):
    publisher = request.get_json()
    validate(instance=publisher, schema=schema)
    update_publisher(publisher_id, publisher)
    return '', 204

@bp.get("")
def get_publishers_endpoint() -> Response:
    publishers = fetch_all_publishers()
    return jsonify(publishers)