# Distributed under the The 3-clause BSD (aka BSD License 2.0)
# See LICENSE.md in repository root for more information.

from flask import Blueprint, Response, jsonify, request
from jsonschema import validate

from erudite.db import get_db
from erudite.error import NotFoundException
from erudite.auth import auth
from erudite.loan import add_new_loan

bp = Blueprint('reservation', __name__, url_prefix='/reservations/')

class ReservationNotFoundException(NotFoundException):
    def __init__(self, idx):
        super().__init__("reservation", idx)


def fetch_pending_reservations():
    db = get_db()
    sql = """
    SELECT id_reservation, receipt_term, submitted, id_catalog, id_user
    FROM reservation
    WHERE receipt_term IS NULL
    """
    db.execute(sql)
    return db.fetchall()

def get_reservations_for_user(user_id: int):
    db = get_db()
    sql = """
        SELECT id_catalog FROM reservation WHERE id_user = %s
        """
    id_catalogs = db.execute(sql, (user_id, )).fetchall()
    books = []
    for id_catalog in id_catalogs:
        sql = """
        SELECT b.id_book, b.title, b.cover FROM book b JOIN catalog c USING(id_book) WHERE c.id_catalog = %s
        """
        db.execute(sql, (id_catalog["id_catalog"],))
        book = db.fetchone()
        sql = """
            SELECT a.id_author, a.name FROM author a
            JOIN book_author ba USING (id_author)
            WHERE ba.id_book = %s
            """
        db.execute(sql, (book["id_book"],))
        book["authors"] = db.fetchall()
        books.append(book)
    return books


def loan_a_reserved_book(reservation_id: int, user_id: int):
    db = get_db()
    sql = """
        SELECT id_catalog FROM reservation WHERE id_user = %s AND id_reservation = %s
        """
    db.execute(sql, (user_id, reservation_id))
    id_catalog = db.fetchone()
    if id_catalog is None:
        raise ReservationNotFoundException(reservation_id)
    client_remove_reservation(reservation_id, user_id)
    add_new_loan(id_catalog["id_catalog"], user_id)

def fetch_single_reservation(reservation_id: int):
    db = get_db()
    sql = """
    SELECT id_reservation, receipt_term, submitted, id_catalog, id_user
    FROM reservation
    WHERE id_reservation = %s
    """
    db.execute(sql, (reservation_id,))
    reservation = db.fetchone()
    if reservation is None:
        raise ReservationNotFoundException(reservation_id)
    return reservation

def client_remove_reservation(reservation_id: int, user_id: int):
    fetch_single_reservation(reservation_id)
    db = get_db()
    sql = """
    DELETE FROM reservation WHERE id_reservation = %s AND id_user = %s
    """
    db.execute(sql, (reservation_id, user_id))


@bp.get('/pending')
def get_pending_reservations_endpoint():
    reservations = fetch_pending_reservations()
    return jsonify(reservations)

@bp.get('/my_reservations')
@auth.login_required(role = 'client')
def get_reservations_for_user_endpoint():
    id_user = auth.current_user()["id_user"]
    reservations = get_reservations_for_user(id_user)
    return jsonify(reservations)

@bp.delete('/<int:reservation_id>')
@auth.login_required(role = 'client')
def client_remove_reservation_endpoint(reservation_id: int):
    id_user = auth.current_user()["id_user"]
    client_remove_reservation(reservation_id, id_user)
    return '', 204

@bp.post('/<int:reservation_id>/loan')
@auth.login_required(role = 'client')
def loan_a_reserved_book_endpoint(reservation_id: int):
    id_user = auth.current_user()["id_user"]
    loan_a_reserved_book(reservation_id, id_user)
    return '', 204