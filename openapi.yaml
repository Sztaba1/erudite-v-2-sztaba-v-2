openapi: '3.0.2'
info:
  title: Erudite API
  version: '0.1'
#servers:
  #- url: https://api.server.test/v1
paths:
  /books/:
    get:
      tags:
        - Finding books
      summary: Get random book from database
      operationId: get_random_book
      parameters:
        - name: limit
          in: query
          schema: 
            type: integer
            format: int64
          description: Limit of number of books returned
      responses:
        200:
          description: Operation success
          content:
            /application/json:
              schema:
                $ref: '#/components/schemas/book_list'
        400:
          $ref: '#/components/responses/invalid_request'
  /books/search:
    get:
      summary: Execute search for books
      tags:
        - Finding books
      parameters:
        - name: s
          in: query
          schema:
            type: string
          description: A single string to search with. If used, other parameters should be empty
        - name: title
          in: query
          schema:
            type: string
          description: With this parameters set, server will make a case insensitive search for given title
        - name: author
          in: query
          schema:
            type: string
          description: With this parameters set, server will make a case insensitive search for given author name
        - name: isbn
          in: query
          schema:
            type: string
          description: Search for a book with exactly this ISBN
      responses:
        200:
          description: Operation success
          content:
            /application/json:
              schema:
                $ref: '#/components/schemas/book_list'
        400:
          $ref: '#/components/responses/invalid_request'
  /books/{id_book}/:
    parameters:
      - name: id_book
        in: path
        schema:
          type: integer
          format: int64
        required: true
    get:
      summary: Get a single book with provided id
      tags:
        - Finding books
      operationId: get_book
      responses:
        200:
          description: Operation success
          content:
            /application/json:
              schema:
                $ref: '#/components/schemas/book'
        400:
          $ref: '#/components/responses/invalid_request'
    post:
      tags:
        - Manipulating books
      summary: Add book to database
      operationId: add_book
      requestBody:
        content:
          /application/json:
            schema:
              type: object
              properties:
                title:
                  type: string
                authors:
                  type: array
                  items:
                    type: integer
                    format: int64
                  description: An array of authors' ids
                isbn:
                  type: string
                  example: 978-3-16-148410-0
                pages:
                  type: integer
                  format: int64
                year:
                  type: integer
                  format: int64
                  example: 1997
                id_publisher:
                  type: integer
                  format: int64
                description:
                  type: string
              required:
                - title
                - authors
                - isbn
                - pages
                - year
        required: true
      responses:
        201: 
          description: Book added successfully
        400:
          $ref: '#/components/responses/invalid_request'
        401:
          $ref: '#/components/responses/unauthenticated'
        403:
          $ref: '#/components/responses/unauthorized'
      security:
        - basicAuth: []
    put:
      summary: Update existing book
      tags:
        - Manipulating books
      operationId: update_book
      requestBody:
        content:
          /application/json:
            schema:
              type: object
              properties:
                isbn:
                  type: string
                  example: 978-3-16-148410-0
                pages:
                  type: integer
                  format: int64
                year:
                  type: integer
                  format: int64
                add_authors:
                  type: array
                  items:
                    type: integer
                    format: int64
                  description: Array of authors' ids
                id_publisher:
                  type: integer
                  format: int64
                description:
                  type: string
                add_tags:
                  type: array
                  items:
                    type: integer
                    format: int64
                  description: An array of tags' ids
        required: true
      responses:
        201:
          description: Operation success
        400:
          $ref: '#/components/responses/invalid_request'
        401:
          $ref: '#/components/responses/unauthenticated'
        403:
          $ref: '#/components/responses/unauthorized'
      security:
        - basicAuth: []
    delete:
      summary: Delete book from database
      tags:
        - Manipulating books
      operationId: delete_book
      responses:
        204:
          description: Operation success
        400:
          $ref: '#/components/responses/invalid_request'
        401: 
          $ref: '#/components/responses/unauthenticated'
        403:
          $ref: '#/components/responses/unauthorized'
      security:
        - basicAuth: []
  /books/{id_book}/upload_cover:
    post:
      summary: Upload books' cover
      tags:
        - Manipulating books
      operationId: upload_book_cover
      parameters:
        - name: id_book
          in: path
          required: true
          schema:
            type: integer
            format: int64
      requestBody:
        description: An image to upload
        content:
          /image/jpeg:
            schema:
              type: string
              format: binary
          /image/png:
            schema:
              type: string
              format: binary
      responses:
        201:
          description: Cover uploaded successfully
        400:
          $ref: '#/components/responses/invalid_request'
        401:
          $ref: '#/components/responses/unauthenticated'
        403:
          $ref: '#/components/responses/unauthorized'
      security:
        - basicAuth: []
  /books/add_with_isbn:
    post:
      summary: Add a new book with provided ISBN using external book index
      tags:
        - Manipulating books
      operationId: add_book_with_isbn
      parameters:
        - name: isbn
          in: query
          required: true
          schema:
            type: string
      responses:
        200:
          description: Operation success
        400:
          $ref: '#/components/responses/invalid_request'
        401:
          $ref: '#/components/responses/unauthenticated'
        403:
          $ref: '#/components/responses/unauthorized'
      security:
        - basicAuth: []
      
  /authors/{id_author}/get_books:
    get:
      summary: Get books written by provided author
      tags:
        - Finding books
      operationId: get_author_books
      parameters:
        - name: id_author
          in: path
          required: true
          schema:
            type: integer
            format: int64
      responses:
        200:
          description: Operation success
          content:
            /application/json:
              schema:
                $ref: '#/components/schemas/book_list'
        400:
          $ref: '#/components/responses/invalid_request'
  /publishers/{id_publisher}/get_books:
    get:
      summary: Get books published by provided publisher
      tags:
        - Finding books
      operationId: get_author_books
      parameters:
        - name: id_publisher
          in: path
          required: true
          schema:
            type: integer
            format: int64
      responses:
        200:
          description: Operation success
          content:
            /application/json:
              schema:
                $ref: '#/components/schemas/book_list'
        400:
          $ref: '#/components/responses/invalid_request'
  /catalog/get_for_book:
    get:
      summary: List catalog for provided book
      tags:
        - Accessing catalog and making reservation
      operationId: get_catalog_for_book
      parameters:
        - name: id_book
          in: query
          required: true
          schema:
            type: integer
            format: int64
      responses:
        200:
          description: Operation success
          content:
            /application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/catalog'
        400:
          $ref: '#/components/responses/invalid_request'
  /catalog/{id_catalog}/reserve:
    post:
      summary: Reserve book from catalog
      tags:
        - Accessing catalog and making reservation
      operationId: reserve_book
      parameters:
        - name: id_catalog
          in: query
          required: true
          schema:
            type: integer
            format: int64
      responses:
        204:
          description: Book successfully reserved
        400:
          $ref: '#/components/responses/invalid_request'
        401:
          $ref: '#/components/responses/unauthenticated'
      security:
        - basicAuth: []
  /authors:
    post:
      summary: Add author to database
      tags:
      - Authors
      operationId: add_author
      requestBody:
        content:
          /application/json:
            schema:
              type: object
              properties:
                name:
                  type: string
              required:
                - name
      responses:
        204:
          description: Author created successfully
        400:
          $ref: '#/components/responses/invalid_request'
        401:
          $ref: '#/components/responses/unauthenticated'
        403:
          $ref: '#/components/responses/unauthorized'
  /authors/{id_author}:
    parameters:
      - name: id_author
        in: path
        required: true
        schema:
          type: integer
          format: int64
    put:
      summary: Update author
      tags:
      - Authors
      operationId: update_author
      requestBody:
        content:
          /application/json:
            schema:
              type: object
              properties:
                name:
                  type: string
      responses:
        204:
          description: Author updated successfully
        400:
          $ref: '#/components/responses/invalid_request'
        401:
          $ref: '#/components/responses/unauthenticated'
        403:
          $ref: '#/components/responses/unauthorized'
      security:
        - basicAuth: []
    delete:
      summary: Delete author
      tags:
        - Authors
      operationId: delete_author
      responses:
        204:
          description: Operation success
        400:
          $ref: '#/components/responses/invalid_request'
        401:
          $ref: '#/components/responses/unauthenticated'
        403:
          $ref: '#/components/responses/unauthorized'
      security:
        - basicAuth: []
  
  /publishers:
    post:
      summary: Add publisher to database
      tags:
      - Publishers
      operationId: add_publisher
      requestBody:
        content:
          /application/json:
            schema:
              type: object
              properties:
                name:
                  type: string
              required:
                - name
      responses:
        204:
          description: Operation success
        400:
          $ref: '#/components/responses/invalid_request'
        401:
          $ref: '#/components/responses/unauthenticated'
        403:
          $ref: '#/components/responses/unauthorized'
  /publishers/{id_publisher}:
    put:
      summary: Update publisher
      tags:
      - Publishers
      operationId: update_publisher
      requestBody:
        content:
          /application/json:
            schema:
              type: object
              properties:
                name:
                  type: string
      responses:
        204:
          description: Operation success
        400:
          $ref: '#/components/responses/invalid_request'
        401:
          $ref: '#/components/responses/unauthenticated'
        403:
          $ref: '#/components/responses/unauthorized'
      security:
        - basicAuth: []
    delete:
      summary: Delete publisher
      tags:
        - Publishers
      operationId: delete_publisher
      responses:
        204:
          description: Operation success
        400:
          $ref: '#/components/responses/invalid_request'
        401:
          $ref: '#/components/responses/unauthenticated'
        403:
          $ref: '#/components/responses/unauthorized'
      security:
        - basicAuth: []

  /loans/pending:
    get:
      summary: Get all loans that are yet to be returned
      tags:
        - Loans
      operationId: get_pending_loans
      responses:
        200:
          description: Operation success
          content:
            /application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/loan'
        401:
          $ref: '#/components/responses/unauthenticated'
        403:
          $ref: '#/components/responses/unauthorized'
      security:
        - basicAuth: []
      
  /loans/my_loans:
    get:
      summary: Get logged in user loans
      tags:
        - Loans
      operationId: get_my_loans
      responses:
        200:
          description: Operation success
          content:
            /application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/loan'
        401:
          $ref: '#/components/responses/unauthenticated'
      security:
        - basicAuth: []
      
  /loans/{id_loan}/prolong:
    post:
      summary: Prolong loan
      tags:
        - Loans
      operationId: loan_prolong
      parameters:
        - name: id_loan
          in: path
          required: true
          schema:
            type: integer
            format: int64
      responses:
        204:
          description: Operation success
        400:
          $ref: '#/components/responses/invalid_request'
        401:
          $ref: '#/components/responses/unauthenticated'
        403:
          description: Not authorized (should be logged in as user who owns provided loan)
      security:
        - basicAuth: []
  /reservations/my_reservation:
    get:
      summary: Get logged in user's reservations
      tags:
      - Reservations
      operationId: get_my_reservations
      responses:
        200:
          description: Operation success
          content:
            /application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/reservation'
        401:
          $ref: '#/components/responses/unauthenticated'
      security:
        - basicAuth: []
      
  /reservations/pending:
    get:
      summary: Get reservations that are yet to be prepared
      tags:
      - Reservations
      operationId: get_pending_reservations
      responses:
        200:
          description: Operation success
          content:
            /application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/reservation'
        401:
          $ref: '#/components/responses/unauthenticated'
        403:
          $ref: '#/components/responses/unauthorized'
      security:
        - basicAuth: []
      
  /reviews/get_for_book:
    get:
      summary: Get all reviews for provided book
      tags:
        - Reviews
      operationId: get_book_reviews
      parameters:
        - name: id_book
          in: query
          schema:
            type: integer
            format: int64
      responses:
        200:
          description: Operation success
          content:
            /application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/review'
        400:
          $ref: '#/components/responses/invalid_request'
  /books/{id_book}/review:
    post:
      summary: Review provided book
      description: Reviews book with id_book and bases review author on logged in user
      tags:
        - Reviews
      operationId: review_book
      parameters:
        - name: id_book
          in: path
          required: true
          schema:
            type: integer
            format: int64
      requestBody:
        content:
          /application/json:
            schema:
              type: object
              properties:
                content:
                  type: string
                score:
                  $ref: '#/components/schemas/score'
      responses:
        201:
          description: Review added successfully
        400:
          $ref: '#/components/responses/invalid_request'
        401:
          $ref: '#/components/responses/unauthenticated'
        403:
          $ref: '#/components/responses/unauthorized'
      security:
        - basicAuth: []
    
  /tags:
    get:
      summary: Get all tags
      tags:
      - Tags
      operationId: get_all_tags
      responses:
        200:
          description: Operation success
          content:
            /application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/tag'
    post:
      summary: Add a new tag
      tags:
        - Tags
      operationId: add_tag
      requestBody:
        content:
          /application/json:
            schema:
              type: object
              properties:
                name:
                  type: string
                color:
                  type: string
              required:
                - name
                - color
      responses:
        204:
          description: Operation success
        400:
          $ref: '#/components/responses/invalid_request'
        401:
          description: Authorization header not provided
        403:
          description: Credentials are wrong
      security:
        - basicAuth: []
      
  /tags/{id_tag}:
    put:
      summary: Update tag
      tags:
        - Tags
      requestBody:
        content:
          /application/json:
            schema:
              type: object
              properties:
                name:
                  type: string
                color:
                  type: string
      responses:
        204:
          description: Operation success
        400:
          $ref: '#/components/responses/invalid_request'
        401:
          $ref: '#/components/responses/unauthenticated'
        403:
          $ref: '#/components/responses/unauthorized'
      security:
        - basicAuth: []
      
    delete:
      summary: Delete tag
      tags:
        - Tags
      responses:
        204:
          description: Operation success
        400:
          $ref: '#/components/responses/invalid_request'
        401:
          $ref: '#/components/responses/unauthenticated'
        403:
          $ref: '#/components/responses/unauthorized'
      security:
        - basicAuth: []
      
  /auth/register:
    post:
      summary: Register new user
      tags:
      - Authorization
      operationId: register_user
      requestBody:
        content:
          /application/json:
            schema:
              type: object
              properties:
                email:
                  type: string
                  format: email
                  example: user@example.com
                password:
                  type: string
      responses:
        204:
          description: User registered successfully
        400:
          $ref: '#/components/responses/invalid_request'
  /auth/check:
    get:
      summary: Check if logged in user credentials are correct
      description: This endpoint will make server look into Authorization header and based email contained in it find user. Then, server will check if provided password matches the one store in database and return appropriate response.
      tags:
        - Authorization
      operationId: auth_check
      responses:
        204:
          description: Credentials are ok
        401:
          description: Authorization header not provided or credentials are wrong
      security:
        - basicAuth: []
      
    
components:
  schemas:
    book:
      type: object
      properties:
        id_book:
          type: integer
          format: int64
        title:
          type: string
        isbn:
          type: string
          example: 978-3-16-148410-0
        authors:
          type: array
          items:
            $ref: '#/components/schemas/author'
        pages:
          type: integer
          format: int64
          example: 365
        year:
          type: integer
          format: int64
          example: 1997
        publisher:
          $ref: '#/components/schemas/publisher'
        description:
          type: string
        cover:
          type: string
          format: url
          example: https://example.com/alsiejfisjalfej.png
        tags:
          type: array
          items:
            $ref: '#/components/schemas/tag'
    book_list:
      type: array
      items:
        type: object
        properties:
          id_book:
            type: integer
            format: int64
          title:
            type: string
          authors:
            type: array
            items:
              $ref: '#/components/schemas/author'
          cover:
            type: string
            format: url
            example: https://example.com/alsiejfisjalfej.png
    publisher:
      type: object
      properties:
        id_publisher:
          type: integer
          format: int64
        name:
          type: string
    author:
      type: object
      properties:
        id_author:
          type: integer
          format: int64
        name:
          type: string
    tag:
      type: object
      properties:
        id_tag:
          type: integer
          format: int64
        name:
          type: string
        color:
          type: string
          format: RGB
    loan:
      type: object
      properties:
        id_loan:
          type: integer
          format: int64
        id_user:
          type: integer
          format: int64
        id_catalog:
          type: integer
          format: int64
        date_of_loan:
          type: string
          format: date
        date_of_return:
          type: string
          format: date
        prolongations:
          type: integer
          format: int64
    reservation:
      type: object
      properties:
        id_reservation:
          type: integer
          format: int64
        id_user:
          type: integer
          format: int64
        id_catalog:
          type: integer
          format: int64
        submitted:
          type: string
          format: date
        receipt_term:
          type: string
          format: date
    catalog:
      type: object
      properties:
        id_catalog:
          type: integer
          format: int64
        id_book:
          type: integer
          format: int64
        shelf_mark:
          type: string
          description: Shelf mark representing physical location of book in library. Its format is undefined and completely library dependant.
    review:
      type: object
      properties:
        id_review:
          type: integer
          format: int64
        id_book:
          type: integer
          format: int64
        id_user:
          type: integer
          format: int64
        content:
          type: string
        score:
          $ref: '#/components/schemas/score'
        timestamp:
          type: string
          format: datetime
    score:
      type: integer
      enum:
        - 1
        - 2
        - 3
        - 4
        - 5
        - 6
        - 7
        - 8
        - 9
        - 10
    error_message:
      type: string
      description: A human readable error message
      example: Limit should be a positive integer.
  securitySchemes:
    basicAuth:
      type: http
      scheme: basic
  responses:
    invalid_request:
      content:
        /text/plain:
          schema:
            $ref: '#/components/schemas/error_message'
      description: Invalid request
    unauthenticated:
      description: Not authenticated
    unauthorized:
      description: Not authorized (librarian role required)
