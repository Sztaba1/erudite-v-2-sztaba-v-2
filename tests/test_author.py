# Distributed under the The 3-clause BSD (aka BSD License 2.0)
# See LICENSE.md in repository root for more information.

from tests.auth import librarian
from erudite.author import search_authors_by_name

def test_getting_books_for_given_author(client):
    response = client.get('/authors/1/get_books')
    assert response.status_code == 200
    books = response.get_json()
    for book in books:
        assert "id_book" in book
        assert "cover" in book
        assert "title" in book
        authors = book["authors"]
        assert authors[0]["name"] == "Andrzej Sapkowski"

def test_adding_author(client):
    author = {
        "name": "Stephen King"
    }
    response = client.post('/authors', json=author, auth=librarian)
    assert response.status_code == 204

def test_adding_invalid_author(client):
    author = {
        "kupa": "Stephen King"
    }
    response = client.post('/authors', json=author, auth=librarian)
    assert response.status_code == 400

def test_updating_author(client):
    author = {
        "name": "Stephen King"
    }
    response = client.put('/authors/1', json=author, auth=librarian)
    assert response.status_code == 204

def test_removing_author(client):
    response = client.delete('/authors/1', auth=librarian)
    assert response.status_code == 204

def test_searching_authors_by_name(app):
    with app.app_context():
        authors = search_authors_by_name("mickiewicz")
    assert authors[0]["name"] == "Adam Mickiewicz"

def test_getting_all_authors(client):
    response = client.get('/authors')
    assert response.status_code == 200
    authors = response.get_json()
    for author in authors:
        assert "id_author" in author
        assert "name" in author