# Distributed under the The 3-clause BSD (aka BSD License 2.0)
# See LICENSE.md in repository root for more information.

from tests.auth import librarian, user

def test_getting_loan(client):
    response = client.get("/loans/1")
    assert response.status_code == 200
    loan = response.get_json()
    assert loan["id_loan"] == 1
    assert loan["id_user"] == 2
    assert loan["id_catalog"] == 6
    assert loan["date_of_loan"] == "2022-12-02"
    assert loan["date_of_return"] == "2023-03-02"
    assert loan["prolongations"] == 4

def test_getting_nonexistent_loan(client):
    response = client.get("/loans/4")
    assert response.status_code == 404

def test_getting_pending_loans(client):
    response = client.get("/loans/pending")
    assert response.status_code == 200

def test_prolong(client):
    response = client.post('/loans/2/prolong', auth=user)
    assert response.status_code == 204

def test_prolong_too_many(client):
    response = client.post('/loans/1/prolong', auth=user)
    assert response.status_code == 405

def test_getting_user_loans(client):
    response = client.get("/loans/my_loans", auth=user)
    assert response.status_code == 200
    loans = response.get_json()
    for book in loans:
        assert "id_book" in book
        assert "title" in book
        assert "cover" in book
        assert "authors" in book
        for author in book["authors"]:
            assert "id_author" in author
            assert "name" in author
def test_getting_user_loans_no_auth(client):
    response = client.get("/loans/my_loans")
    assert response.status_code == 401

def test_removing_loan(client):
    response = client.delete("/loans/1", auth=librarian)
    assert response.status_code == 204