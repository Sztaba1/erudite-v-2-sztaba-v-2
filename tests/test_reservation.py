# Distributed under the The 3-clause BSD (aka BSD License 2.0)
# See LICENSE.md in repository root for more information.

from auth import user

def test_getting_pending_reservations(client):
    response = client.get('/reservations/pending')
    assert response.status_code == 200
    reservations = response.get_json()
    for reservation in reservations:
        assert reservation["receipt_term"] is None
        assert "id_reservation" in reservation
        assert "id_user" in reservation
        assert "id_catalog" in reservation
        assert "submitted" in reservation

def test_getting_reservations_for_user(client):
    response = client.get('/reservations/my_reservations', auth=user)
    assert response.status_code == 200
    reservation = response.get_json()
    for book in reservation:
        assert "id_book" in book
        assert "title" in book
        assert "cover" in book
        assert "authors" in book
        for author in book["authors"]:
            assert "id_author" in author
            assert "name" in author


def test_removing_reservation(client):
    response = client.delete('/reservations/2', auth=user)
    assert response.status_code == 204

def test_removing_nonexistent_reservation(client):
    response = client.delete('/reservations/10', auth=user)
    assert response.status_code == 404


def test_removing_reservation_no_auth(client):
    response = client.delete('/reservations/1')
    assert response.status_code == 401

def test_loaning_out_a_reserved_book(client):
    response = client.post('/reservations/2/loan', auth=user)
    assert response.status_code == 204