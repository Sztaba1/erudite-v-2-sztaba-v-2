# Distributed under the The 3-clause BSD (aka BSD License 2.0)
# See LICENSE.md in repository root for more information.

from tests.auth import librarian

def test_getting_all_tags(client):
    response = client.get('/tags')
    assert response.status_code == 200
    tags = response.get_json()
    counter = 0
    for tag in tags:
        assert "name" in tag
        assert "color" in tag
        counter += 1
    assert counter == 7

def test_adding_tag(client):
    tag = {
        "name": "kolor",
        "color": "#629A14"
    }
    response = client.post('/tags', json=tag, auth=librarian)
    assert response.status_code == 204

def test_adding_invalid_tag(client):
    tag = {
        "kupa": "kolor",
        "color": "#629A14"
    }
    response = client.post('/tags', json=tag, auth=librarian)
    assert response.status_code == 400

def test_delete_tag(client):
    response = client.delete('/tags/1', auth=librarian)
    assert response.status_code == 204

def test_update_tag(client):
    tag = {
        "name": "kolor",
        "color": "#629A14"
    }
    response = client.put('/tags/1', json=tag, auth=librarian)
    assert response.status_code == 204